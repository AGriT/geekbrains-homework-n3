﻿#include <iostream>

extern int nA, nB, nC, nD;

const int ncnstA = 214, ncnstB = 345, ncnstC = 26, ncnstD = 598;

float fResult1, fResult3;

int nCube[3][3][3]{};

const int ncnstComparedNumber = 31, ncnstCompare = 21;

int main()
{
	fResult1 = ncnstA * (ncnstB + ((float)ncnstC / ncnstD));
	std::cout << fResult1 << '\n';

	// part 2

	std::cout << ((ncnstComparedNumber > ncnstCompare) ? (ncnstComparedNumber - ncnstCompare) * 2 : ncnstCompare - ncnstComparedNumber) << '\n';

	// part 3

	fResult3 = nA * (nB + ((float)nC / nD));
	std::cout << fResult3 << '\n';

	// part 4

	int* pnCubeCenter = &nCube[1][1][1];

	std::cout << *pnCubeCenter << '\n';
}
